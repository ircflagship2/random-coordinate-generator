var coords = [];

function inShape(coord, shape) {

    function leafLetPolyToCoords(polygon) {
        return $.map(polygon, function(latlng) {
            return [[ latlng.lat, latlng.lng ]];
        });
    }

    var inAnyLayer = false;
    shape.eachLayer(function(layer) {
        $.each(layer.getLatLngs(), function(key, polygon) {
            polygon = leafLetPolyToCoords(polygon);
            if (inside(coord, polygon )) {
                inAnyLayer = true;
            }
        });
    });
    return inAnyLayer;
}

function addRandomCoords(shape, bounds, amount) {
    for(var i = 0; i < amount; i++) {
        var isInShape = false;

        while (!isInShape) {

            //generate random coords
            var lat = (Math.random() * (bounds.getSouth() - bounds.getNorth()) + bounds.getNorth());
            var lng = (Math.random() * (bounds.getWest() - bounds.getEast()) + bounds.getEast());

            //check if within shape
            isInShape = inShape([ lat, lng ], shape);
            //console.log ( lat + "," + lng + ": " + isInShape );

            if (isInShape) {
                var marker = L.marker([lat, lng]).addTo(map);
                coords.push([lat,lng]);
            }
        }
    }
}