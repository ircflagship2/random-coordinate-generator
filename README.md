# Generate Random On-Land Coordinates in JS
Small couple of js functions that allows you to generate random coordinates within a LeafLet shape. The code can easily be changed to work with geoJSON coordinates for instance.

Also contains a small python script for converting shp files to geoJSON. 

##Thanks
- [Leaflet.js](https://github.com/Leaflet/Leaflet)
- [point-in-polygon](https://github.com/substack/point-in-polygon)
- [pyshp](https://github.com/substack/point-in-polygon) 
- [world.geo.json](https://github.com/johan/world.geo.json)

 